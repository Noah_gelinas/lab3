package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void echoTest() {
        assertEquals("testing echo method",5,App.echo(5));
    }

    @Test
    public void oneMoreTest() {
        assertEquals("testing oneMore method",6,App.oneMore(5));
    }
}
